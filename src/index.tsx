import React from "react";
import ReactDOM from "react-dom/client";
import "./app/styles/base/index.css";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import MainPage from "./pages/MainPage";
import { Routes } from "./shared/lib/utils/routes";
import StoryPage from "./pages/StoryPage";
import NotFound from "./pages/NotFound";
import Header from "./shared/ui/components/molecules/Header";

const router = createBrowserRouter([
  {
    path: Routes.MAIN,
    element: <Header />,
    errorElement: <NotFound />,
    children: [
      { path: Routes.MAIN, element: <MainPage /> },
      { path: Routes.STORY, element: <StoryPage /> },
    ],
  },
]);

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
