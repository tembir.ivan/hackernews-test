import React from "react";
import style from "./index.module.css";
import { Story } from "../../../../../entities/types";

const Comment = ({ title, text, time, by }: Story) => {
  const date = new Date(time);

  return (
    <div className={style.wrapper}>
      <div className={style.info}>
        <span className={style.by}>By {by}</span>
        <span className={style.time}>
          {date.toDateString() + " " + date.toLocaleTimeString()}
        </span>
      </div>
      <span className={style.title}>{title}</span>
      <p className={style.text}>{text}</p>
    </div>
  );
};

export default Comment;
