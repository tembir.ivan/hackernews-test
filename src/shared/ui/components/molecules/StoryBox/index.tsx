import React from "react";
import style from "./index.module.css";
import { Link } from "react-router-dom";
import { Story } from "../../../../../entities/types";

const StoryBox = ({ id, title, text, time }: Story) => {
  const date = new Date(time).toLocaleDateString();

  return (
    <Link to={`story/${id}`} className={style.wrapper}>
      <span className={style.title}>{title}</span>
      <p className={style.text}>{text}</p>
      <span className={style.date}>{date}</span>
    </Link>
  );
};

export default StoryBox;
