import React from "react";
import style from "./index.module.css";
import LogoIcon from "../../atoms/Icons/LogoIcon";
import { Link, Outlet, useParams } from "react-router-dom";
import { useAppDispatch } from "../../../../../entities/hooks";
import { getAllStories } from "../../../../lib/utils/getData";

const Header = () => {
  const param = useParams();
  const dispatch = useAppDispatch();

  const handleClick = () => {
    getAllStories({ dispatch });
  };
  return (
    <>
      <header>
        <div className={`content ${style.wrapper}`}>
          <Link to="/" className={style.logo}>
            <LogoIcon width={24} height={27} />
            News Site
          </Link>
          {!param.id && (
            <button className={style.reload} onClick={handleClick}>
              Reload
            </button>
          )}
        </div>
      </header>
      <main>
        <Outlet />
      </main>
    </>
  );
};

export default Header;
