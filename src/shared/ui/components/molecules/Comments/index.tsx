import React from "react";
import Comment from "../../atoms/Comment";
import { Story } from "../../../../../entities/types";

const Comments = ({ comments }: Story) => {
  return (
    <div>
      <h2>Comments</h2>
      {comments?.length ? (
        <>
          {comments.map((data) => (
            <React.Fragment key={data.id}>
              <Comment {...data} />
            </React.Fragment>
          ))}
        </>
      ) : (
        <div>Empty</div>
      )}
    </div>
  );
};

export default Comments;
