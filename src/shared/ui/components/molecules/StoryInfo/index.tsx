import React from "react";
import style from "./index.module.css";
import { Story } from "../../../../../entities/types";

const StoryInfo = ({ by, time, url, title, text }: Story) => {
  const date = new Date(time);

  return (
    <div className={style.wrapper}>
      <a href={url} target="_blank" className={style.url} rel="noreferrer">
        {url}
      </a>
      <div className={style.info}>
        <span className={style.by}>By {by}</span>
        <span className={style.time}>
          {date.toDateString() + " " + date.toLocaleTimeString()}
        </span>
      </div>
      <h1>{title}</h1>
      <p className={style.text}>{text}</p>
    </div>
  );
};

export default StoryInfo;
