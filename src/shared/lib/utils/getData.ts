import { Story } from "../../../entities/types";
import { setData } from "../../../redux/features/storiesSlice";

export const getAllStories = ({
  dispatch,
}: {
  dispatch: (...params: any) => void;
}) => {
  fetch("https://hacker-news.firebaseio.com/v0/newstories.json?print=pretty")
    .then((response) => response.json())
    .then((data) => {
      const promises = [];
      for (let i = 0; i < 100; i++) {
        promises.push(
          new Promise((resolve, reject) => {
            fetch(
              `https://hacker-news.firebaseio.com/v0/item/${data[i]}.json?print=pretty`
            )
              .then((response) => response.json())
              .then((result: Story) => {
                resolve(result);
              });
          })
        );
      }

      Promise.all(promises).then((result) => {
        dispatch(setData({ stories: result }));
      });
    });
};

export const getStory = ({
  dispatch,
  id,
}: {
  dispatch: (...params: any) => void;
  id: string;
}) => {
  fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`)
    .then((response) => response.json())
    .then((data) => {
      const story: Story = { ...data };
      const promises: Promise<Story>[] = [];
      if (story.kids) {
        story.kids.forEach((elem: number) => {
          promises.push(
            new Promise((resolve, reject) => {
              fetch(
                `https://hacker-news.firebaseio.com/v0/item/${elem}.json?print=pretty`
              )
                .then((response) => response.json())
                .then((result: Story) => {
                  resolve(result);
                });
            })
          );
        });
      }
      Promise.all(promises).then((result) => {
        story.comments = [...result];
        dispatch(setData({ currentStory: story }));
      });
    });
};
