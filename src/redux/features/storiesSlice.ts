import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "../store";
import { Story } from "../../entities/types";

type StoryStore = {
  stories: Story[];
  currentStory?: Story;
};

const initialState: StoryStore = {
  stories: [] as Story[],
  currentStory: undefined,
};

export const storiesSlice = createSlice({
  name: "stories",
  initialState,
  reducers: {
    setData: (state, action) => {
      state.stories = action.payload.stories
        ? [...action.payload.stories]
        : [...state.stories];
      state.currentStory = action.payload.currentStory;
    },
  },
});

export const { setData } = storiesSlice.actions;

export default storiesSlice.reducer;
