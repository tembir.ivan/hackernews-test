import React, { useEffect } from "react";
import style from "./index.module.css";
import { useAppDispatch, useAppSelector } from "../../entities/hooks";
import { getAllStories } from "../../shared/lib/utils/getData";
import { Story } from "../../entities/types";
import StoryBox from "../../shared/ui/components/molecules/StoryBox";
import { setData } from "../../redux/features/storiesSlice";

const MainPage = () => {
  const stories = useAppSelector((state) => state.stories.stories);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(setData({}));
    getAllStories({ dispatch });
    const intervalId = setInterval(() => {
      getAllStories({ dispatch });
    }, 3600000);

    return () => {
      clearInterval(intervalId);
    };
  }, [dispatch]);

  return (
    <section>
      <div className={`content ${style.wrapper}`}>
        {stories.map((data: Story) => (
          <React.Fragment key={data.id}>
            <StoryBox {...data} />
          </React.Fragment>
        ))}
      </div>
    </section>
  );
};

export default MainPage;
