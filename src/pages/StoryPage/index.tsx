import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import style from "./index.module.css";
import { useAppDispatch, useAppSelector } from "../../entities/hooks";
import { getStory } from "../../shared/lib/utils/getData";
import StoryInfo from "../../shared/ui/components/molecules/StoryInfo";
import { Story } from "../../entities/types";
import Comments from "../../shared/ui/components/molecules/Comments";

const StoryPage = () => {
  const currentStory: Story | undefined = useAppSelector(
    (state) => state.stories.currentStory
  );
  const dispatch = useAppDispatch();
  const route = useParams();

  useEffect(() => {
    getStory({ dispatch, id: route.id! });
  }, []);

  return (
    <section>
      <div className={`content ${style.wrapper}`}>
        {currentStory === undefined ? (
          <>Loading...</>
        ) : (
          <>
            <StoryInfo {...currentStory!} />
            <Comments {...currentStory!} />
          </>
        )}
      </div>
    </section>
  );
};

export default StoryPage;
