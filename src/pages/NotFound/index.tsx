import { Link, useRouteError } from "react-router-dom";
import style from "./index.module.css";

export default function NotFound() {
  const error = useRouteError();
  console.error(error);

  return (
    <div className={style.wrapper}>
      <h1>Oops!</h1>
      <p>Sorry, an unexpected error has occurred.</p>
      <Link to={"/"}>Back to the Main Page</Link>
    </div>
  );
}
